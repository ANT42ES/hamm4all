CREATE TYPE coords AS (
    x INTEGER,
    y INTEGER
);

ALTER TABLE Thumbnail_Link
    ADD COLUMN coordinates coords;

CREATE TABLE temp AS SELECT id_seq, order_num, id_level, x, y FROM Thumbnail_Link_Laby;
TRUNCATE TABLE Thumbnail_Link_Laby;

INSERT INTO Thumbnail_Link
    SELECT id_seq, order_num, id_level, ROW(x, y)::coords FROM temp;

DROP TABLE Thumbnail_Link_Laby;
TRUNCATE TABLE temp;
DROP TABLE temp;