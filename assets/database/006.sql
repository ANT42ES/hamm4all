ALTER TABLE Lands
    RENAME COLUMN has_difficulty TO has_variable_difficulty;

UPDATE Lands
    SET has_variable_difficulty = NOT has_variable_difficulty;

ALTER TABLE Lands
    ALTER COLUMN has_variable_difficulty SET DEFAULT FALSE;

ALTER TABLE Changelog
    ALTER COLUMN date TYPE Timestamp with time zone;

----------------------------------

ALTER TABLE Lands
    RENAME COLUMN created_at TO released_at;