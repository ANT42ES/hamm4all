UPDATE Lands SET difficulty = NULL;

ALTER TABLE Lands
    DROP COLUMN difficulty;

ALTER TABLE Lands
    ADD COLUMN has_difficulty boolean DEFAULT True;

UPDATE Lands SET has_difficulty = True;

CREATE TABLE Land_Graduation (
    id_land integer,
    id_user integer,
    grade real CHECK (grade >= 0 AND grade <= 10),
    primary key (id_land, id_user)
);