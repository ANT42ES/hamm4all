CREATE TABLE Roles ( -- CHECK !
    id SERIAL,
    nom varchar NOT NULL,
    primary key (id)
);

CREATE TABLE Experience_Levels ( -- CHECK !
    id SERIAL,
    experience integer NOT NULL,
    primary key (id)
);

CREATE TABLE Users (
    id SERIAL,
    name varchar NOT NULL UNIQUE,
    password bytea NOT NULL,
    role integer NOT NULL DEFAULT 1,
    created_at timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    experience integer NOT NULL DEFAULT 0,
    primary key (id),
    foreign key (role) REFERENCES Roles(id)
);

CREATE TABLE Lands ( -- CHECK !
    id SERIAL,
    name varchar NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    description varchar,
    difficulty real CHECK (difficulty >= 0 AND difficulty <= 10),
    primary key (id)
);

CREATE TABLE Authors ( -- CHECK !
    id SERIAL,
    name varchar NOT NULL,
    primary key (id)
);

CREATE TABLE Land_Authors ( -- CHECK !
    id_author integer NOT NULL,
    id_land integer NOT NULL,
    primary key (id_author, id_land)
);

CREATE TABLE Region (
    id SERIAL,
    id_land integer references lands(id),
    name varchar NOT NULL,
    primary key (id)
);

CREATE TABLE Levels ( -- CHECK !
    id SERIAL,
    land_id integer NOT NULL,
    name varchar NOT NULL,
    id_region integer references Region(id),
    description varchar,
    previous_id integer,
    next_id integer,
    primary key (id),
    foreign key (land_id) references Lands(id),
    foreign key (previous_id) references Levels(id),
    foreign key (next_id) references Levels(id) 
);

CREATE TABLE Dimension ( -- CHECK !
    id_base_level integer NOT NULL,
    id_destination_level integer NOT NULL,
    name varchar,
    facing varchar,
    primary key (id_base_level, id_destination_level),
    foreign key (id_base_level) REFERENCES Levels(id),
    foreign key (id_destination_level) REFERENCES Levels(id)
);

CREATE TABLE Land_Part ( -- CHECK !
    id SERIAL,
    name varchar NOT NULL,
    id_land integer references Lands(id) NOT NULL,
    difficulty real CHECK (difficulty >= 0 AND difficulty <= 10),
    description varchar,
    primary key (id)
);

CREATE TABLE Thumbnail_Sequence ( -- CHECK !
    id SERIAL,
    order_num SERIAL,
    id_start integer NOT NULL references Levels(id),
    id_land_part integer NOT NULL references Land_Part(id),
    primary key (id)
);

CREATE TABLE Thumbnail_Link ( -- CHECK !
    id_seq SERIAL references Thumbnail_Sequence(id),
    order_num SERIAL,
    id_level integer NOT NULL references Levels(id),
    primary key (id_seq, order_num)
);

CREATE TABLE Thumbnail_Link_Laby ( -- CHECK !
    x integer NOT NULL,
    y integer NOT NULL
) INHERITS (Thumbnail_Link);

CREATE TABLE Changelog (
    name varchar NOT NULL,
    date DATE DEFAULT CURRENT_DATE,
    changes varchar[] NOT NULL,
    primary key (name)
);