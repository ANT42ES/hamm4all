DROP TABLE Experience_Levels;

CREATE TABLE Fruits (
    id SERIAL,
    name VARCHAR NOT NULL,
    image varchar, 
    primary key (id)
);

CREATE TABLE Orders (
    id SERIAL,
    id_level integer NOT NULL REFERENCES Levels(id),
    id_fruit integer NOT NULL REFERENCES Fruits(id),
    order_num integer DEFAULT 1 CHECK (order_num > 0),
    x real NOT NULL CHECK (x >= 0 AND x < 20),
    y real NOT NULL CHECK (y >= 0 AND y < 25),
    modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    modified_by integer REFERENCES Users(id),
    primary key (id)
);

CREATE TABLE Descriptions (
    id SERIAL,
    id_level integer NOT NULL REFERENCES Levels(id),
    content varchar,
    modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    modified_by integer REFERENCES Users(id),
    primary key (id)
);

CREATE TABLE videos (
    id SERIAL,
    link varchar NOT NULL,
    has_order boolean NOT NULL DEFAULT FALSE,
    id_level integer NOT NULL REFERENCES Levels(id),
    modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    modified_by integer REFERENCES Users(id),
    primary key (id)
);

ALTER TABLE Levels
    DROP COLUMN description;

ALTER TABLE Levels
    ADD COLUMN created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    ADD COLUMN modified_by integer REFERENCES Users(id);




