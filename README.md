# Hamm4all

Hamm4all is a helping website for Hammerfest and Eternalfest.
This site is made to help Hammerfest and Eternalfest players, by holding screens and orders of the more levels.

## 1. Installation

**WARNING**: The site is currently being upgraded to a new major version. So many Merge Requests may be rejected due to the new layout organization I'm trying to make.

### 1.1 prerequisites

* Nodejs : https://nodejs.org/en/
* git : https://git-scm.com/downloads
* postgreSQL : https://www.postgresql.org/download/

### 1.2 Clone

First of all, you need to clone the repo and the corresponding database :
```bash
git clone https://gitlab.com/FireKing54/hamm4all.git
git clone https://gitlab.com/FireKing54/hamm4all-db.git
```

### 1.3 Setup Database

Create the database :
```bash
su postgres
psql
CREATE DATABASE hamm4all WITH ENCODING utf8;
```

**WARNING** Restoring database process is not working yet

Then `CTRL + D` to end connection to psql, and restore the database using :
```bash
cd hamm4all-db
psql -U postgres hamm4all < "save.sql"
```

Finally, go to the `hamm4all` directory and setup connection JSON :
```bash
cd ../hamm4all
cp .connect.json.default .connect.json
```
and then fill up the file to be able to connect to hamm4all database.

### 1.4 Install dependencies

To install dependences, run `npm install` OR `npm ci`

## 2 Run the website

To run the website, you only need to do

```bash
node index.js
```

Then, you would be able to get the site with http://localhost:8080

## 3 Link

https://hamm4all.net