class Level {
    public lvl: number;
    public xp: number;
    public before: number;

    constructor(l: number, xp: number, b: number) {
        this.lvl = l;
        this.xp = xp;
        this.before = b;
    }
}

export { Level };