import path from "path";

import express from "express"; // Charge express
import session from "cookie-session"; // Charge le middleware de sessions
import bodyParser from "body-parser"; // Charge le middleware de gestion des paramètres
import fileUpload from 'express-fileupload'; // Charge le middleware d'upload
const urlencodedParser = bodyParser.urlencoded({ extended: false });

import { 
    GET,
    MainPage,
    Lands,
    Changelog,
    ConnectPage,
    Logout as GETLogout,
    UserProfile,
    Technics,
    Crystals,
    Letters,
    CacheEnnemi,
    Orders,
    Bugs,
    Fruits,
    Boss,
    SpecialObjects,
    Families,
    Quests,
    Coefficients,
    Pads,
    Keys,
    CreatedLevels,
    Musics,
    Contests,
    Tools,
    Thanks,
    LandParts,
    LevelsOfPart,
    Levels,
    ControlLevels,
    Users
} from "./pages/GET";

import { 
    POST, 
    Logout as POSTLogout, 
    Login, 
    Signup, 
    ChangelogAdding, 
    ScreenAdding, 
    DescriptionAdding,
    VideoAdding, 
    LinkLevelAdding, 
    DimLevelAdding, 
    DimLevelDelete, 
    FruitLevelAdd, 
    FruitLevelDelete, 
    MoveLevelOrder, 
    LandsAdding, 
    LandsModify, 
    LandsDelete, 
    LandsThumbnailAdding, 
    LandsGradeAdding, 
    PartAdding, 
    PartModify, 
    PartDelete, 
    SequenceAdding, 
    SequenceModify, 
    SequenceDelete, 
    SequenceMove, 
    ThumbnailLinkAdding, 
    ThumbnailLinkRemove, 
    LevelAdding, 
    LevelModify, 
    LevelDelete, 
    MoveScreenOrder, 
    ProfilePictureAdding,
    LevelRandomizer
} from "./pages/POST";

class App {

    public app: express.Application;
    private readonly API_LINK: string = "/api";

    constructor() {
        this.app = express();
        this.config();
        this.enableRoutes();
        this.listen();
    }

    private config(): void {
        this.app.use(express.json())
        this.app.use(fileUpload());
        this.app.use(session({secret: "hamm4all"}));
        this.app.use(express.static(path.join(__dirname, '../assets')));
        this.app.use(express.static(path.join(__dirname, '../../hamm4all-db')));
    }

    private enableRoutes(): void {
        const GETRoutes: Array<GET> = [
            new MainPage(), new Lands(), new Changelog(), new ConnectPage(), new GETLogout(), new UserProfile(),
            new Technics(), new Crystals(), new Letters(), new CacheEnnemi(), new Orders(), new Bugs(),
            new Fruits(), new Boss(), new SpecialObjects(), new Families(), new Quests(), new Coefficients(),
            new Pads(), new Keys(), new CreatedLevels(), new Musics(), new Contests(), new Tools(), new Thanks(),
            new LandParts(), new LevelsOfPart(), new Levels(), new ControlLevels(), new Users()

        ];
        const POSTRoutes: Array<POST> = [
            new POSTLogout(), new Login(), new Signup(), new ChangelogAdding(), new ScreenAdding(),
            new DescriptionAdding(), new VideoAdding(), new LinkLevelAdding("previous", "/levels/previous/:land/:level/add"),
            new LinkLevelAdding("next", "/levels/next/:land/:level/add"), new DimLevelAdding(),
            new DimLevelDelete(), new FruitLevelAdd(), new FruitLevelDelete(),
            new MoveLevelOrder("+", "/levels/order/:land/:level/upgrade/:id"),
            new MoveLevelOrder("-", "/levels/order/:land/:level/downgrade/:id"), new LandsAdding(),
            new LandsModify(), new LandsDelete(), new LandsThumbnailAdding(), new LandsGradeAdding(),
            new PartAdding(), new PartModify(), new PartDelete(), new SequenceAdding(), new SequenceModify(),
            new SequenceDelete(), new SequenceMove("+", "/sequences/template/:land/:part/:id/upgrade"),
            new SequenceMove("-", "/sequences/template/:land/:part/:id/downgrade"), new ThumbnailLinkAdding(),
            new ThumbnailLinkRemove(), new LevelAdding(), new LevelModify, new LevelDelete(),
            new MoveScreenOrder("1", "/levels/screen_order/:land/:level/upgrade/:index"),
            new MoveScreenOrder("-1", "/levels/screen_order/:land/:level/downgrade/:index"),
            new ProfilePictureAdding(), new LevelRandomizer()
        ];

        for(let route of GETRoutes) {
            this.app.get(route.LINK, (req, res) => route.loadview(req, res, route.PAGE));
            this.app.get(this.API_LINK + route.LINK, (req, res) => route.loadAPI(req, res));
        }
        for(let route of POSTRoutes) {
            this.app.post(route.LINK, urlencodedParser, (req, res) => route.loadview(req, res));
            this.app.post(this.API_LINK + route.LINK, urlencodedParser, (req, res) => route.loadAPI(req, res));
        }

        // Default route if the page is not found
        this.app.use((req, res) => res.status(404).render("ERROR_NOT_FOUND.pug"));
    }

    private listen() : void {
        this.app.listen(process.env.PORT || 8080);
    }
}

new App();








