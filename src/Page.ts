import { Request, Response } from "express";
import { ErrorCode} from "./pages/modules/ErrorCode";

interface API {
    [index: string]: any,
    [index: number]: any
}

interface APIError {
    "error": true;
    "message": string;
    "error_id": number;
}

abstract class Page {

    readonly LINK: string;
    readonly OPTION: Array<string>;

    constructor(options?: string | Array<string>, link?: string) {
        if (options) {
            this.OPTION = (options instanceof Array) ? options : [options];
        }
        if (link) {
            this.LINK = link;
        }
    }

    public abstract async getAPI(req: Request, res: Response): Promise<API>;
    public abstract async loadAPI(req: Request, res: Response): Promise<void>;
    public abstract async loadview(req: Request, res: Response, page: string): Promise<void>;

    protected isAPIError(thing: any): thing is APIError {
        return (thing && thing.error === true && typeof thing.message === "string" && typeof thing.error_id === "number")
    }

    protected hasNecessaryRole(role: {role: number} | number | undefined, needed: number): boolean {
        if (role === undefined) return false;
        else if (role instanceof Object) {
            return role.role >= needed;
        }
        return role >= needed;
    }

    protected async apiErrorCheck(res: Response, api: API) {
        if (this.isAPIError(api)) {
            this.apiError(res, api);
            return true;
        }
        return false;
    }

    protected async apiError(res: Response, error: APIError) {
        res.status(ErrorCode.get(error.error_id).HTTPCode).send(error);
    }

}

export {
    Page,
    API,
    APIError,
}