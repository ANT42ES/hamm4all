import { Page, API, APIError } from "../Page";
import {Request, Response} from "express";
import { AuthToken, AuthTokenInfo } from "./modules/AuthToken";
import { request_one_element } from "blessed-request";

abstract class POST extends Page {

    protected REDIRECT: string = "/";

    public async loadAPI(req: Request, res: Response): Promise<void> {
        if (req.body.token) {
            const info: AuthTokenInfo | APIError = await AuthToken.getTokenInfo(req.body.token);
            if (await this.apiErrorCheck(res, info)) {}
            else {
                req.session.user = {};
                req.session.user.id = (info as AuthTokenInfo).playerID;
                req.session.user.name = (info as AuthTokenInfo).playerName;
                req.session.user.role = (info as AuthTokenInfo).role;
            }
        }
        const API: API = await this.getAPI(req, res);
        if (! await this.apiErrorCheck(res, API)) {
            res.status(200).send((Object.keys(API).length > 0) ? API : "Everything went good")
        }
    }

    public abstract async getAPI(req: Request, res: Response): Promise<API>;

    public async loadview(req: Request, res: Response): Promise<void> {
        const API: API = await this.getAPI(req, res);
        if(! this.isAPIError(API)) {
            res.redirect(this.REDIRECT);
        }
    }

    public async fetchLevelID(id_land: number | string, level_name: string): Promise<number | undefined> {
        return request_one_element("SELECT id FROM Levels WHERE land_id = $1::integer AND name = $2", [id_land, level_name]);
    }

}

export {
    POST
}

export * from "./POST/Logout";
export * from "./POST/Login";
export * from "./POST/Signup";
export * from "./POST/ChangelogAdding";
export * from "./POST/ScreenAdding";
export * from "./POST/DescriptionAdding";
export * from "./POST/VideoAdding";
export * from "./POST/LinkLevelAdding";
export * from "./POST/DimLevelAdding";
export * from "./POST/DimLevelDelete";
export * from "./POST/FruitLevelAdd";
export * from "./POST/FruitLevelDelete";
export * from "./POST/MoveLevelOrder";
export * from "./POST/LandsAdding";
export * from "./POST/LandsModify";
export * from "./POST/LandsDelete";
export * from "./POST/LandsThumbnailAdding";
export * from "./POST/LandsGradeAdding";
export * from "./POST/PartAdding";
export * from "./POST/PartModify";
export * from "./POST/PartDelete";
export * from "./POST/SequenceAdding";
export * from "./POST/SequenceModify";
export * from "./POST/SequenceDelete";
export * from "./POST/SequenceMove";
export * from "./POST/ThumbnailLinkAdding";
export * from "./POST/ThumbnailLinkRemove";
export * from "./POST/LevelAdding";
export * from "./POST/LevelModify";
export * from "./POST/LevelDelete";
export * from "./POST/MoveScreenOrder";
export * from "./POST/ProfilePictureAdding";
export * from "./POST/LevelRandomizer";