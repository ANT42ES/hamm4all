import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import * as difficulty from "../modules/difficulty";
import * as sequence from "../modules/sequence";

class PartAdding extends POST {

    LINK = "/parts/template/:land/add";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.body.name, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.difficulty, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const exist: boolean = (await request("SELECT Count(*) FROM Land_Part WHERE name = $1::varchar AND id_land = $2::integer", [req.body.name, req.params.land])) > 0;
        if (exist) return ErrorCode.getAPIError(this.LINK, 13);

        const diff: number = await difficulty.difficulty(req.body.difficulty);
        const ind: number = await sequence.get(await request("SELECT id FROM Land_Part"));

        request(`INSERT INTO Land_Part VALUES($1::integer, $2::varchar, $3::integer, $4::real, $5::varchar, DEFAULT, DEFAULT, $6::integer)`, [
            ind, req.body.name, req.params.land, diff, req.body.description, req.session.user.id
        ]);

        logsuccess(`The part ${req.body.name} has successfully been created !`);

        return {};
    }
}

export {
    PartAdding
}