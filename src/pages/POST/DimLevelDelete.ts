import { request, request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";

class DimLevelDelete extends POST {

    LINK = "/levels/dimension/:land/:level/delete";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (! this.hasNecessaryRole(req.session.user, 3)) return ErrorCode.getAPIError(this.LINK, 2);
        if (await isUndefined(req.body.dim, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const id_destination = await this.fetchLevelID(req.params.land, req.body.dim);
        const id_level = await this.fetchLevelID(req.params.land, req.params.level);
        const exists: boolean = (await request_one_element("SELECT Count(*) FROM Dimension WHERE id_destination_level = $1::integer AND id_base_level = $2::integer", [id_destination, id_level])) == 1;

        if (exists) {
            request("DELETE FROM Dimension WHERE id_base_level = $1::integer AND id_destination_level = $2::integer", [id_level, id_destination]);
        }
        else return ErrorCode.getAPIError(this.LINK, 13);

        logsuccess(`The dimension ${req.body.link} has successfully been deleted for level "${req.params.level}" of land "${req.params.land}" !`);

        return {};
    }
}

export {
    DimLevelDelete
}