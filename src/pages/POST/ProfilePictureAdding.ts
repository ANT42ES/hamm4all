import fs from "fs";
import path from "path";
import { request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import * as upload from "../modules/upload";

class ProfilePictureAdding extends POST {

    LINK = "/profile/screen/:user/add";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/profile/${req.params.user}`;

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);
        if (req.session.user.id != req.params.user && req.session.user.role < 3) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.params.user, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.user, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);
        if (await isUndefined(req.files, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.files.screen, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        let screen = req.files.screen instanceof Array ? req.files.screen[0] : req.files.screen;

        const MAX_UPLOAD_SIZE = 1 * 1024 * 1024;

        if (screen.size > MAX_UPLOAD_SIZE) return ErrorCode.getAPIError(this.LINK, 14);
        if(screen.name.split(".")[screen.name.split(".").length - 1].toLowerCase() !== "png") return ErrorCode.getAPIError(this.LINK, 15);

        let id: number | undefined = await request_one_element("SELECT id FROM Users WHERE id = $1::integer", [req.params.user]);
        if (! id) return ErrorCode.getAPIError(this.LINK, 12);

        const pathFile = path.join(__dirname, `../../../../hamm4all-db/users`);
        
        if (! fs.existsSync(pathFile)) {
            fs.mkdirSync(pathFile);
        }
        if (! fs.existsSync(`${pathFile}/${id}`)) {
            fs.mkdirSync(`${pathFile}/${id}`);
        }

        upload.resize(screen.data, 512, 512, `${pathFile}/${id}/image.png`,
            `The profile picture has successfully been uploaded for user "${id}"`,
            `An error has occured while writing the profile picture on the server`);

        return {};
    }
}

export {
    ProfilePictureAdding
}