import fs from "fs";
import path from "path";
import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";

class LevelModify extends POST {

    LINK = "/levels/template/:land/modify";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels`;

        if (! this.hasNecessaryRole(req.session.user, 3)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.body.old, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.new, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const existOld: boolean = (await request("SELECT Count(*) FROM Levels WHERE land_id = $1::integer AND name = $2::varchar", [req.params.land, req.body.old])) > 0;
        if (! existOld) return ErrorCode.getAPIError(this.LINK, 13);

        const existNew: boolean = (await request("SELECT Count(*) FROM Levels WHERE land_id = $1::integer AND name = $2::varchar", [req.params.land, req.body.new])) > 0;
        if (existNew) return ErrorCode.getAPIError(this.LINK, 13);

        request(`UPDATE Levels SET name = $1::varchar, modified_at = DEFAULT, modified_by = $2::integer WHERE name = $3::varchar AND land_id = $4::integer`, [
            req.body.new, req.session.user.id, req.body.old, req.params.land
        ]);

        try {
            fs.renameSync(path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/${req.body.old}.png`),
                        path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/${req.body.new}.png`));
            fs.renameSync(path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/min/${req.body.old}.png`),
                        path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/min/${req.body.new}.png`));
        }
        catch (err) {}
        

        logsuccess(`The level ${req.body.old} of land ${req.params.land} has successfully been renamed to ${req.body.new} !`);

        return {};
    }
}

export {
    LevelModify
}