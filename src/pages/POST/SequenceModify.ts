import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";

class SequenceModify extends POST {

    LINK = "/sequences/template/:land/:part/modify";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (await isUndefined(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const id = (await request("SELECT id FROM land_part WHERE id_land = $1::integer", [req.params.land])).indexOf(parseInt(req.params.part));
        this.REDIRECT = `/lands/${req.params.land}/parts/${id}`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);
        if (await isUndefined(req.body.thumb, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);;
        if (await isUndefined(req.body.level, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);;

        const existThm: boolean = (await request("SELECT Count(*) FROM Thumbnail_Sequence WHERE id = $1::integer", [req.body.thumb])) > 0;
        if (! existThm) return ErrorCode.getAPIError(this.LINK, 13);

        const exist: boolean = (await request("SELECT Count(*) FROM Levels WHERE name = $1::varchar AND land_id = $2::integer", [req.body.level, req.params.land])) > 0;
        if (! exist) return ErrorCode.getAPIError(this.LINK, 13);

        const idLevel : number = await this.fetchLevelID(req.params.land, req.body.level);

        request(`UPDATE Thumbnail_Sequence SET id_start = $1::integer, modified_at = DEFAULT, modified_by = $2::integer WHERE id = $3::integer`, [
            idLevel, req.session.user.id, req.body.thumb
        ])

        logsuccess(`The sequence has successfully been modified !`);

        return {};
    }
}

export {
    SequenceModify
}