import { request, request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";

class FruitLevelDelete extends POST {

    LINK = "/levels/fruits/:land/:level/delete";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);
        if (await isUndefined(req.body.fruit, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        let splited = req.body.fruit.split(" ");

        const id_fruit: number | undefined = await request_one_element("SELECT id FROM Fruits WHERE name = $1", [splited[0]]);
        if (! id_fruit) return ErrorCode.getAPIError(this.LINK, 13);

        splited = splited[1].split(";");

        const x = parseFloat(splited[0].split("(")[1]);
        const y = parseFloat(splited[1].split(")")[0]);

        if (await isNotANumber(`${x}`, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);
        if (await isNotANumber(`${y}`, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        if (x < 0 || x >= 20 || y < 0 || y >= 25) return ErrorCode.getAPIError(this.LINK, 18);

        const id_level: number | undefined = await this.fetchLevelID(req.params.land, req.params.level);

        if (! id_level) return ErrorCode.getAPIError(this.LINK, 13);

        request("DELETE FROM Orders WHERE x = $1::real AND y = $2::real AND id_fruit = $3::integer AND id_level = $4::integer", [x, y, id_fruit, id_level])
        logsuccess(`The fruit "${req.body.fruit}" has successfully been deleted from level "${req.params.level}" of land "${req.params.land}" !`);

        return {};
    }
}

export {
    FruitLevelDelete
}