import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";
import * as XP from "../modules/xp";

class VideoAdding extends POST {

    private readonly XP_VIDEO = 12;

    LINK = "/levels/video/:land/:level/add";

    public async getAPI(req: Request, res: Response): Promise<API> {


        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);
        if (await isUndefined(req.body.link, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const id = await this.fetchLevelID(req.params.land, req.params.level);
        const exists: boolean = (await request("SELECT Count(*) FROM Videos WHERE id_level = $1::integer", [id])) == 1;

        const link: string = req.body.link.includes("/embed") ? req.body.link : req.body.link.split("watch?v=").join("embed/");

        if(exists) {
            request("UPDATE Videos SET link = $1, modified_at = DEFAULT, modified_by = $2::integer, has_order = $3::boolean WHERE id_level = $4::integer", [link, req.session.user.id, (req.body.order === "on"), id]);
        }
        else {
            request("INSERT INTO Videos VALUES (DEFAULT, $1, $2, $3::integer, DEFAULT, $4::integer)", [link, (req.body.order === "on"), id, req.session.user.id])
        }

        XP.add(req.session.user.id, this.XP_VIDEO);
        logsuccess(`The video link has successfully been uploaded for level "${req.params.level}" of land "${req.params.land}" !`);

        return {};
    }
}

export {
    VideoAdding
}