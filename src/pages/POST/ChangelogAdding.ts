
import { request, request_one_object, request_one_element, request_full } from "blessed-request";
import { POST } from "../POST";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";

class ChangelogAdding extends POST {

    LINK = "/changelog/adding";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (! this.hasNecessaryRole(req.session.user, 5)) return ErrorCode.getAPIError(this.LINK, 2);
        if (await isUndefined(req.body.name, "/changelog", res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.changes, "/changelog", res)) return ErrorCode.getAPIError(this.LINK, 12);

        const changes: Array<string> = req.body.changes.split("\r\n");

        await request("INSERT INTO Changelog VALUES ($1::varchar, $2, $3)", [
            req.body.name, `'${new Date().toISOString()}'`, changes
        ])

        this.REDIRECT = "/";
        return {}
        
    }
}

export {
    ChangelogAdding
}