import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import * as sequence from "../modules/sequence";

class ThumbnailLinkRemove extends POST {

    LINK = "/links/template/:land/:part/remove";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (await isUndefined(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const id = (await request("SELECT id FROM land_part WHERE id_land = $1::integer", [req.params.land])).indexOf(parseInt(req.params.part));
        this.REDIRECT = `/lands/${req.params.land}/parts/${id}`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);
        if (await isUndefined(req.body.sequence, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const exist: boolean = (await request("SELECT Count(*) FROM Thumbnail_link WHERE id_seq = $1::integer", [req.body.sequence])) > 0;
        if (! exist) return ErrorCode.getAPIError(this.LINK, 13);

        const order: number = await sequence.get(await request("SELECT order_num FROM Thumbnail_Link WHERE id_seq = $1::integer", [req.body.sequence]));

        if (order !== 1) {
            request("DELETE FROM Thumbnail_Link WHERE id_seq = $1::integer AND order_num = $2::integer", [
                req.body.sequence, order - 1 
            ]);

            logsuccess(`The link has successfully been removed !`);
        }

        return {};
    }
}

export {
    ThumbnailLinkRemove
}