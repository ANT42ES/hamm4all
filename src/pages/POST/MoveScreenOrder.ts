import fs from "fs";
import path from "path";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import Hashids from "hashids";

class MoveScreenOrder extends POST {

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;
        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);

        if (await isUndefined(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.params.level, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);
        if (await isUndefined(req.params.index, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.index, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const hashids = new Hashids(req.params.land + req.params.level, 6);
        const ind: number = (parseInt(req.params.index) - 1);

        if (await isNotANumber(`${ind}`, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const pathPart = path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/${req.params.level}`);
        const pathPartMin = path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/min/${req.params.level}`);
        let screenPath = ".png";
        let i = 0;

        while (fs.existsSync(pathPart + screenPath)) {
            i++;
            const encoded = hashids.encode(i)
            screenPath = `_${encoded}.png`;
        }
        i--;
        const modif: number = parseInt(this.OPTION[0]);

        if (ind < 0 || ind + modif < 0 || ind > i || ind + modif > i) return ErrorCode.getAPIError(this.LINK, 18);

        let thisHash = hashids.encode(ind);
        if (ind === 0) thisHash = undefined;
        let otherHash = hashids.encode(ind + modif);
        if (ind + modif === 0) otherHash = undefined;


        if (fs.existsSync(pathPart + ((thisHash) ? `_${thisHash}.png` : ".png")) && fs.existsSync(pathPart + ((otherHash) ? `_${otherHash}.png` : ".png"))) {
            if (ind === 0) {
                fs.renameSync(pathPart + `.png`, pathPart + "temp.png");
                fs.renameSync(pathPart +  `_${otherHash}.png`, pathPart + `.png`);
                fs.renameSync(pathPart + `temp.png`, pathPart +  `_${otherHash}.png`);
                fs.renameSync(pathPartMin + `.png`, pathPartMin + "temp.png");
                fs.renameSync(pathPartMin +  `_${otherHash}.png`, pathPartMin + `.png`);
                fs.renameSync(pathPartMin + `temp.png`, pathPartMin +  `_${otherHash}.png`);
            }
            else if (ind + modif === 0) {
                fs.renameSync(pathPart + `_${thisHash}.png`, pathPart + "temp.png");
                fs.renameSync(pathPart +  `.png`, pathPart + `_${thisHash}.png`);
                fs.renameSync(pathPart + "temp.png", pathPart +  `.png`);
                fs.renameSync(pathPartMin + `_${thisHash}.png`, pathPartMin + "temp.png");
                fs.renameSync(pathPartMin +  `.png`, pathPartMin + `_${thisHash}.png`);
                fs.renameSync(pathPartMin + "temp.png", pathPartMin +  `.png`);
            }
            else {
                fs.renameSync(pathPart + `_${thisHash}.png`, pathPart + "temp.png");
                fs.renameSync(pathPart +  `_${otherHash}.png`, pathPart + `_${thisHash}.png`);
                fs.renameSync(pathPart + "temp.png", pathPart +  `_${otherHash}.png`);
                fs.renameSync(pathPartMin + `_${thisHash}.png`, pathPartMin + "temp.png");
                fs.renameSync(pathPartMin +  `_${otherHash}.png`, pathPartMin + `_${thisHash}.png`);
                fs.renameSync(pathPartMin + "temp.png", pathPartMin +  `_${otherHash}.png`);
            }
        }

        logsuccess("The screen order has successfully been modified !")

        return {};
    }
}

export {
    MoveScreenOrder
}