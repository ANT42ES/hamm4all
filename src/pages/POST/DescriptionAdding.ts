import { request, request_one_object, request_one_element, request_full } from "blessed-request";
import { POST } from "../POST";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { logwarning, logsuccess } from "blessed-logcolor";
import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import * as XP from "../modules/xp";

class DescriptionAdding extends POST {

    private readonly XP_DESCRIPTION = 10;

    LINK = "/levels/description/:land/:level/add";

    public async getAPI(req: Request, res: Response): Promise<API> {

        const MIN_LENGTH: number = 10;

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);

        if (await isUndefined(req.params.level, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);
        if (await isUndefined(req.body.description, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        if (req.body.description.length < MIN_LENGTH) return ErrorCode.getAPIError(this.LINK, 16);

        const id = await this.fetchLevelID(req.params.land, req.params.level);
        const exists: boolean = (await request_one_element("SELECT Count(*) FROM Descriptions WHERE id_level = $1::integer", [id])) == 1;

        if(exists) {
            request("UPDATE Descriptions SET content = $1, modified_at = DEFAULT, modified_by = $2::integer WHERE id_level = $3::integer", [req.body.description, req.session.user.id, id]);
        }
        else {
            request("INSERT INTO Descriptions VALUES (DEFAULT, $1::integer, $2, DEFAULT, $3::integer)", [id, req.body.description, req.session.user.id])
        }

        XP.add(req.session.user.id, this.XP_DESCRIPTION);
        logsuccess(`The description has successfully been uploaded for level "${req.params.level}" of land "${req.params.land}" !`);

        return {};
    }
}

export {
    DescriptionAdding
}