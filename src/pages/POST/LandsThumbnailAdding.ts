import path from "path";
import { request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";
import * as upload from "../modules/upload";

class LandsThumbnailAdding extends POST {

    LINK = "/lands/template/thumbnail";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands`;
        if (! this.hasNecessaryRole(req.session.user, 3)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.files, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.files.thumb, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        // Fetch the id of the land
        const id: number = await request_one_element("SELECT id FROM Lands WHERE name = $1::varchar", [req.body.land]);
        if (await isUndefined(id, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const thumb = req.files.thumb instanceof Array ? req.files.thumb[0] : req.files.thumb;

        const MAX_UPLOAD_SIZE = 1 * 1024 * 1024;

        if (thumb.size > MAX_UPLOAD_SIZE) return ErrorCode.getAPIError(this.LINK, 14);
        if(await upload.verify(thumb.name)) return ErrorCode.getAPIError(this.LINK, 15);

        const pathFile = path.join(__dirname, `../../../../hamm4all-db/lands/${id}/image.png`);
        upload.resize(thumb.data, 200, 200, pathFile,
            `The Thumbnail has successfully been uploaded to land ${req.body.land}`,
            `The Thumbnail has not been successfully uploaded to land ${req.body.land}`);

        return {};
    }
}

export {
    LandsThumbnailAdding
}