import fs from "fs";
import path from "path";
import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";
import * as sequence from "../modules/sequence";

class LandsAdding extends POST {

    LINK = "/lands/template/add";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.body.name, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.difficulty, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const exist: boolean = (await request("SELECT Count(*) FROM Lands WHERE name = $1::varchar", [req.body.name])) > 0;
        if (exist) return ErrorCode.getAPIError(this.LINK, 13);

        const ind: number = await sequence.get(await request("SELECT id FROM Lands"));

        request(`INSERT INTO Lands VALUES($1::integer, $2::varchar, DEFAULT, NULL, DEFAULT, $3::integer, $4::boolean)`, [
            ind, req.body.name, req.session.user.id, req.body.difficulty === "Variable"
        ]);

        try {
            fs.mkdirSync(path.join(__dirname, `../../../../hamm4all-db/lands/${ind}`));
            fs.mkdirSync(path.join(__dirname, `../../../../hamm4all-db/lands/${ind}/min`));
        } catch {
            return ErrorCode.getAPIError(this.LINK, 6);
        }

        logsuccess(`The land ${req.body.name} has successfully been created !`);

        return {};
    }
}

export {
    LandsAdding
}