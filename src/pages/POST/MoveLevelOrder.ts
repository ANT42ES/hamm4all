import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";

class MoveLevelOrder extends POST {

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);

        const exists: boolean = (await request("SELECT Count(*) FROM Orders WHERE id = $1::integer", [req.params.id])) == 1;
        if (exists) {
            if (this.OPTION[0] === "+"){
                request("UPDATE Orders SET order_num = order_num + 1, modified_at = DEFAULT, modified_by = $1::integer WHERE id = $2::integer", [req.session.user.id, req.params.id]);
            }
            else if (this.OPTION[0] === "-") {
                request("UPDATE Orders SET order_num = order_num - 1, modified_at = DEFAULT, modified_by = $1::integer WHERE id = $2::integer", [req.session.user.id, req.params.id]);
            }
            else return ErrorCode.getAPIError(this.LINK, 0);
        }
        else return ErrorCode.getAPIError(this.LINK, 13);

        logsuccess("The order has successfully been modified !")
        return {};
    }
}

export {
    MoveLevelOrder
}