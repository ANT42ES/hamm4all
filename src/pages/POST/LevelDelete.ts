import fs from "fs";
import path from "path";
import { request, request_one_object, request_one_element, request_full } from "blessed-request";
import { POST } from "../POST";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { logwarning, logsuccess } from "blessed-logcolor";
import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";


class LevelDelete extends POST {

    LINK = "/levels/template/:land/remove";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels`;

        if (! this.hasNecessaryRole(req.session.user, 3)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.body.name, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const exist: boolean = (await request("SELECT Count(*) FROM Levels WHERE land_id = $1::integer AND name = $2::varchar", [req.params.land, req.body.name])) > 0;
        if (! exist) ErrorCode.getAPIError(this.LINK, 13);

        request(`DELETE FROM Levels WHERE name = $1::varchar AND land_id = $2::integer`, [
            req.body.name, req.params.land
        ]);

        try {
            const TEMP_PATH = path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/${req.body.name}.png`);
            const TEMP_PATH_THUMBNAIL = path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}/min/${req.body.name}.png`);
            if (fs.existsSync(TEMP_PATH)) fs.unlinkSync(TEMP_PATH);
            if (fs.existsSync(TEMP_PATH_THUMBNAIL)) fs.unlinkSync(TEMP_PATH_THUMBNAIL);
        }
        catch (err) {
            return ErrorCode.getAPIError(this.LINK, 6); 
        }

        logsuccess(`The level ${req.body.name} of land ${req.params.land} has successfully been removed !`);

        return {};
    }
}

export {
    LevelDelete
}