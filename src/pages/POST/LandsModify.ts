import { request, request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";

class LandsModify extends POST {

    LINK = "/lands/template/modify";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.body.old, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const existOld: boolean = (await request_one_element("SELECT Count(*) FROM Lands WHERE name = $1::varchar", [req.body.old])) > 0;
        if (! existOld) return ErrorCode.getAPIError(this.LINK, 13);

        if (req.body.description) {
            request(`UPDATE Lands SET description = $1::varchar, modified_at = DEFAULT, modified_by = $2::integer WHERE name = $3::varchar`, [
                req.body.description, req.session.user.id, req.body.old
            ]);
        }

        if (req.body.authors) {
            const id_land = await request_one_element("SELECT id FROM Lands WHERE name = $1::varchar", [req.body.old]);
            await request(`DELETE FROM Land_authors WHERE id_land = $1::integer`, [id_land]);
            for (let author of (req.body.authors instanceof Array) ? req.body.authors : [req.body.authors]) {
                
                const id_author = await request_one_element("SELECT id FROM Authors WHERE name = $1::varchar", [author]);
                await request("INSERT INTO Land_Authors VALUES ($1::integer, $2::integer)", [id_author, id_land]);
            }
            await request(`UPDATE Lands SET modified_at = DEFAULT, modified_by = $1::integer WHERE name = $2::varchar`, [
                req.session.user.id, req.body.old
            ]);
        }

        if (req.body.new) {
            const existNew: boolean = (await request_one_element("SELECT Count(*) FROM Lands WHERE name = $1::varchar", [req.body.new])) > 0;
            if (existNew) return ErrorCode.getAPIError(this.LINK, 13);
            request(`UPDATE Lands SET name = $1::varchar, modified_at = DEFAULT, modified_by = $2::integer WHERE name = $3::varchar`, [
                req.body.new, req.session.user.id, req.body.old
            ]);
        }

        logsuccess(`The land ${req.body.old} has successfully been modified !`);

        return {};
    }
}

export {
    LandsModify
}