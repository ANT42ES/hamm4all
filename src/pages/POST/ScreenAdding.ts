import path from "path";
import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import * as XP from "../modules/xp";
import * as upload from "../modules/upload";
import Hashids from "hashids";

class ScreenAdding extends POST {


    private readonly XP_SCREEN = 10;
    LINK = "/levels/screen/:land/:level/add/:num_screen";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (await isUndefined(req.files, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.files.screen, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const screen = req.files.screen instanceof Array ? req.files.screen[0] : req.files.screen;
        const MAX_UPLOAD_SIZE = 1 * 1024 * 1024;

        if (screen.size > MAX_UPLOAD_SIZE) return ErrorCode.getAPIError(this.LINK, 14);
        if(screen.name.split(".")[screen.name.split(".").length - 1].toLowerCase() !== "png") return ErrorCode.getAPIError(this.LINK, 15);

        request("UPDATE Levels SET modified_at = DEFAULT, modified_by = $1::integer WHERE name = $2 AND land_id = $3::integer", [req.session.user.id, req.params.level, req.params.land]);

        if (req.params.num_screen === undefined) req.params.num_screen = "1";
        if ((await isNotANumber(req.params.num_screen, this.REDIRECT, res))) return ErrorCode.getAPIError(this.LINK, 11);
        const num_screen: number = parseInt(req.params.num_screen, 10) - 1;
        const hashids = new Hashids(req.params.land + req.params.level, 6);

        const pathFile = path.join(__dirname, `../../../../hamm4all-db/lands/${req.params.land}`);
        const level = req.params.level + ((num_screen > 0) ? `_${hashids.encode(num_screen)}`: "");

        upload.resize(screen.data, 420, 500, pathFile + `/${level}.png`,
            `The screen has successfully been uploaded for level "${req.params.level}" of land "${req.params.land}" !`,
            `An error has occured while writing the screen on the server`);

        upload.resize(screen.data, 100, 119, pathFile + `/min/${level}.png`,
            `The screen has successfully been resized for level "${req.params.level}" of land "${req.params.land}" !`,
            `The screen of level "${req.params.level}" of land "${req.params.land}" has not been resized correctly`);

        XP.add(req.session.user.id, this.XP_SCREEN);
        
        return {};
        
    }
}

export {
    ScreenAdding
}