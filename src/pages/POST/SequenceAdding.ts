import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import * as sequence from "../modules/sequence";

class SequenceAdding extends POST {

    LINK = "/sequences/template/:land/:part/add";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const id = (await request("SELECT id FROM land_part WHERE id_land = $1::integer", [req.params.land])).indexOf(parseInt(req.params.part));
        this.REDIRECT = `/lands/${req.params.land}/parts/${id}`;

        if (await isUndefined(req.body.level, this.REDIRECT, res)) return;

        const exist: boolean = (await request("SELECT Count(*) FROM Levels WHERE name = $1::varchar AND land_id = $2::integer", [req.body.level, req.params.land])) > 0;
        if (! exist) return ErrorCode.getAPIError(this.LINK, 13);

        const ind: number = await sequence.get(await request("SELECT id FROM Thumbnail_sequence"));
        const idLevel : number = await this.fetchLevelID(req.params.land, req.body.level);

        request(`INSERT INTO Thumbnail_Sequence VALUES($1::integer, $2::integer, $3::integer, $4::integer, DEFAULT, DEFAULT, $5::integer)`, [
            ind, 1, idLevel, req.params.part, req.session.user.id
        ]);
        logsuccess(`The sequence has successfully been created !`);

        return {};
    }
}

export {
    SequenceAdding
}