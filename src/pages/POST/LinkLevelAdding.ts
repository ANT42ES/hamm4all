import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";

class LinkLevelAdding extends POST {

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (! this.hasNecessaryRole(req.session.user, 3)) return ErrorCode.getAPIError(this.LINK, 2);

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;
        if (await isUndefined(req.body.link, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const id: number | undefined = await this.fetchLevelID(req.params.land, req.body.link);

        if (this.OPTION[0] === "previous") request("UPDATE Levels SET previous_id = $1::integer WHERE name = $2 AND land_id = $3::integer", [id, req.params.level, req.params.land]);
        else if (this.OPTION[0] === "next") request("UPDATE Levels SET next_id = $1::integer WHERE name = $2 AND land_id = $3::integer", [id, req.params.level, req.params.land]);
        else return ErrorCode.getAPIError(this.LINK, 17);

        logsuccess(`The ${this.OPTION[0]} link has successfully been uploaded for level "${req.params.level}" of land "${req.params.land}" !`);
        return {};
    }
}

export {
    LinkLevelAdding
}