import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";

class SequenceDelete extends POST {

    LINK = "/sequences/template/:land/:part/remove";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (await isUndefined(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.part, `/lands/${req.params.land}`, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const id = (await request("SELECT id FROM land_part WHERE id_land = $1::integer", [req.params.land])).indexOf(parseInt(req.params.part));
        this.REDIRECT = `/lands/${req.params.land}/parts/${id}`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);
        if (await isUndefined(req.body.thumb, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const exist: boolean = (await request("SELECT Count(*) FROM Thumbnail_Sequence WHERE id = $1::integer", [req.body.thumb])) > 0;
        if (! exist) return ErrorCode.getAPIError(this.LINK, 13);

        request(`DELETE FROM Thumbnail_Sequence WHERE id = $1::integer`, [req.body.thumb]);

        logsuccess(`The sequence has successfully been removed !`);

        return {};
    }
}

export {
    SequenceDelete
}