import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";
import * as difficulty from "../modules/difficulty";

class PartModify extends POST {

    LINK = "/parts/template/:land/modify";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.body.old, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.new, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.difficulty, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const existOld: boolean = (await request("SELECT Count(*) FROM Land_Part WHERE name = $1::varchar AND id_land = $2::integer", [req.body.old, req.params.land])) > 0;
        if (! existOld) return ErrorCode.getAPIError(this.LINK, 13);

        const existNew: boolean = (await request("SELECT Count(*) FROM Land_Part WHERE name = $1::varchar AND id_land = $2::integer", [req.body.new, req.params.land])) > 0;
        if (existNew) return ErrorCode.getAPIError(this.LINK, 13);

        const diff: number = await difficulty.difficulty(req.body.difficulty);

        request(`UPDATE Land_Part SET name = $1::varchar, description = $2::varchar, modified_at = DEFAULT, modified_by = $3::integer, difficulty = $4::real WHERE name = $5::varchar AND id_land = $6::integer`, [
            req.body.new, req.body.description, req.session.user.id, diff, req.body.old, req.params.land
        ]);

        logsuccess(`The part ${req.body.old} has successfully been renamed to ${req.body.new} !`);

        return {};
    }
}

export {
    PartModify
}