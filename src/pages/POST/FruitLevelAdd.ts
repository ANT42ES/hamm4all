import { request, request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";

class FruitLevelAdd extends POST {

    LINK = "/levels/fruits/:land/:level/add";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);
        if (await isUndefined(req.body.fruit, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.x, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isUndefined(req.body.y, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.body.x, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);
        if (await isNotANumber(req.body.y, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const x = parseFloat(req.body.x);
        const y = parseFloat(req.body.y)

        if (x < 0 || x >= 20 || y < 0 || y >= 25) return ErrorCode.getAPIError(this.LINK, 18);

        const id_level: number | undefined = await this.fetchLevelID(req.params.land, req.params.level);
        const id_fruit: number | undefined = await request_one_element("SELECT id FROM Fruits WHERE name = $1", [req.body.fruit]);

        if (! id_fruit) return ErrorCode.getAPIError(this.LINK, 18);

        request("INSERT INTO Orders VALUES(DEFAULT, $1::integer, $2::integer, $3::integer, $4::real, $5::real, DEFAULT, $6::integer)", [id_level, id_fruit, 1, x, y, req.session.user.id])

        logsuccess(`The fruit "${req.body.fruit}" has successfully been added to level "${req.params.level}" of land "${req.params.land}" !`);

        return {};
    }
}

export {
    FruitLevelAdd
}