import { request, request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";

class LandsGradeAdding extends POST {

    LINK = "/lands/template/:land/grade";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}`;

        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);
        if (await isUndefined(req.body.grade, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        // Fetch the id of the land
        const name: number = await request_one_element("SELECT name FROM Lands WHERE id = $1::integer", [req.params.land]);
        if (await isUndefined(name, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 13);
        if (await isUndefined(req.body.grade, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const diff: number = parseFloat(req.body.grade);

        if (diff < 0 || diff > 10) return ErrorCode.getAPIError(this.LINK, 18);

        const exists: number = await request_one_element("SELECT Count(*) FROM Land_graduation WHERE id_land = $1::integer AND id_user = $2::integer", [req.params.land, req.session.user.id]);

        if (exists > 0) {
            request("UPDATE Land_graduation SET grade = $1::real WHERE id_land = $2::integer AND id_user = $3::integer", [diff, req.params.land, req.session.user.id]);
        }
        else {
            request("INSERT INTO Land_graduation VALUES ($1::integer, $2::integer, $3::real)", [req.params.land, req.session.user.id, diff]);
        }

        logsuccess(`The difficulty grade for user ${req.session.user.name} of land ${name} has successfully been updated !`);

        return {};
    }
}

export {
    LandsGradeAdding
}