import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";

class LandsDelete extends POST {

    LINK = "/lands/template/remove";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands`;

        if (! this.hasNecessaryRole(req.session.user, 4)) return ErrorCode.getAPIError(this.LINK, 2);
        if (await isUndefined(req.body.name, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const exist: boolean = (await request("SELECT Count(*) FROM Lands WHERE name = $1::varchar", [req.body.name])) > 0;
        if (! exist) return ErrorCode.getAPIError(this.LINK, 13);

        request(`DELETE FROM Lands WHERE name = $1::varchar`, [req.body.name]);

        logsuccess(`The land ${req.body.name} has successfully been removed !`);

        return {};
    }
}

export {
    LandsDelete
}