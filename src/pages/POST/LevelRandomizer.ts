import { request, request_one_element, request_full } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";

class LevelRandomizer extends POST {

    LINK = "/levels/randomize";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands`;
        const levels: Array<{name: string, land_id: string}> = await request_full("SELECT name, land_id FROM Levels");
        const chosen = levels[Math.floor(Math.random() * levels.length)];
        this.REDIRECT = `/lands/${chosen.land_id}/levels/${encodeURIComponent(chosen.name)}`;

        logsuccess(`Level successfully randomized ! You got on level ${chosen.name} in land ${chosen.land_id}`)

        return {};
    }
}

export {
    LevelRandomizer
}