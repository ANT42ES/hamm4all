
import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";

class Logout extends POST {

    LINK = "/connect";

    public async getAPI(req: Request, res: Response): Promise<API> {
        req.session.user = undefined
        return {};
    }

}

export {
    Logout
}