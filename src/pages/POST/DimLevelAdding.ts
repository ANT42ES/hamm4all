import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined } from "../modules/verify";

class DimLevelAdding extends POST {

    LINK = "/levels/dimension/:land/:level/add";

    public async getAPI(req: Request, res: Response): Promise<API> {


        this.REDIRECT = `/lands/${req.params.land}/levels/${encodeURIComponent(req.params.level)}`;

        if (! this.hasNecessaryRole(req.session.user, 3)) return ErrorCode.getAPIError(this.LINK, 2);
        if (await isUndefined(req.body.link, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);

        const id_destination = await this.fetchLevelID(req.params.land, req.body.link);
        const id_level = await this.fetchLevelID(req.params.land, req.params.level);
        const exists: boolean = (await request("SELECT Count(*) FROM Dimension WHERE id_destination_level = $1::integer AND id_base_level = $2::integer", [id_destination, id_level])) == 1;

        const facing: string | undefined = this.getFacing(req.body.facing);

        if(exists) return ErrorCode.getAPIError(this.LINK, 13);
        else {
            request("INSERT INTO Dimension VALUES ($1::integer, $2::integer, $3, $4)", [id_level, id_destination, req.body.name, facing])
        }

        logsuccess(`The dimension link ${req.body.link} has successfully been uploaded for level "${req.params.level}" of land "${req.params.land}" !`);

        return {};
    }

    private getFacing (face: string): string {
        switch(face) {
            case "Haut":
                return "up";
            case "Gauche":
                return "left";
            case "Droite":
                return "right";
            case "Bas":
                return "down";
            default:
                return undefined;
        }
    }
}

export {
    DimLevelAdding
}