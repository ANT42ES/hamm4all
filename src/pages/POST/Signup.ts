import { request, request_one_element } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";

import * as encrypt from "blessed-encrypt";
import { ErrorCode } from "../modules/ErrorCode";
import { AuthToken } from "../modules/AuthToken";

class Signup extends POST {

    LINK = "/signup";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = "/connect";
        const count: number = await request_one_element("SELECT Count(*) FROM Users WHERE name = $1::varchar", [req.body.username]);
        if(count > 0) return ErrorCode.getAPIError(this.LINK, 8);

        const passwd = await encrypt.getSaltedHash(req.body.password);

        await request("INSERT INTO Users VALUES (DEFAULT, $1::varchar, $2::bytea, DEFAULT, DEFAULT, DEFAULT)", [
            req.body.username, passwd
        ]);

        req.session.user = await request_one_element("SELECT id, name, role FROM Users WHERE name = $1::varchar", [req.body.username]);

        const response: string = await AuthToken.generate();
        const err = await AuthToken.save(response, req.session.user.id, req.session.user.name, req.session.user.role);
        if (this.isAPIError(err)) {
            return err;
        }
        this.REDIRECT = "/";
        return {"token": response };

        
    }
}

export {
    Signup
}