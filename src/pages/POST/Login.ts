import fs from "fs";
import path from "path";
import { request_one_object } from "blessed-request";

import { POST } from "../POST";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";

import * as encrypt from "blessed-encrypt";
import { ErrorCode } from "../modules/ErrorCode";
import { AuthToken } from "../modules/AuthToken";
import { ConnectPage } from "../GET/ConnectPage";

class Login extends POST {

    LINK = "/login";

    public async getAPI(req: Request, res: Response): Promise<API> {
        const redirectPreviousPage: string = (req.query.previousPage as string);
        this.REDIRECT = (redirectPreviousPage) ? `${new ConnectPage().LINK}?previousPage=${redirectPreviousPage}` : `/connect`

        const connectElems: {id: number, password: Buffer} = await request_one_object("SELECT id, password FROM Users WHERE name = $1::varchar", [req.body.username]);
        if(! connectElems) return ErrorCode.getAPIError(this.LINK, 3);

        if(await encrypt.verify(connectElems.password, req.body.password)) {
            req.session.user = await request_one_object("SELECT id, name, role FROM Users WHERE id = $1::integer", [connectElems.id]);
            if (fs.existsSync(path.join(__dirname, `../../../../hamm4all-db/users/${req.session.user.id}/image.png`))) {
                req.session.user.profile_picture = `/users/${req.session.user.id}/image.png`
            }
            else {
                req.session.user.profile_picture = "/assets/min/unknown.png";
            }

            const exists: boolean | APIError = await AuthToken.exists(req.session.user.name);
            if (this.isAPIError(exists)) {
                return exists;
            }
            else if (exists) {
                const token: string | APIError = await AuthToken.getTokenByPlayerName(req.session.user.name);
                if (this.isAPIError(token)) {
                    return token;
                }
                this.REDIRECT = (redirectPreviousPage) ? redirectPreviousPage : `/`;
                return { "token": token }
            }
            const response: string = await AuthToken.generate();
            const err = await AuthToken.save(response, req.session.user.id, req.session.user.name, req.session.user.role);
            if (this.isAPIError(err)) {
                return err;
            }
            return {"token": response };
        }

        this.REDIRECT = (redirectPreviousPage) ? redirectPreviousPage : `/`;
        return ErrorCode.getAPIError(this.LINK, 4);
    }

}

export {
    Login
}