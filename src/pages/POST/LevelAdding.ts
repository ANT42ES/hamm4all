import { request } from "blessed-request";

import { POST } from "../POST";
import { API } from "../../Page";
import { Request, Response } from "express";
import { logsuccess } from "blessed-logcolor";

import { ErrorCode } from "../modules/ErrorCode";
import { isUndefined, isNotANumber } from "../modules/verify";

class LevelAdding extends POST {

    LINK = "/levels/template/:land/add";

    public async getAPI(req: Request, res: Response): Promise<API> {

        this.REDIRECT = `/lands/${req.params.land}/levels`;

        if (! this.hasNecessaryRole(req.session.user, 3)) return ErrorCode.getAPIError(this.LINK, 2);

        if (await isUndefined(req.body.name, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 12);
        if (await isNotANumber(req.params.land, this.REDIRECT, res)) return ErrorCode.getAPIError(this.LINK, 11);

        const exist: boolean = (await request("SELECT Count(*) FROM Levels WHERE land_id = $1::integer AND name = $2::varchar", [req.params.land, req.body.name])) > 0;
        if (exist) return ErrorCode.getAPIError(this.LINK, 13);

        request(`INSERT INTO Levels VALUES(DEFAULT, $1::integer, $2::varchar, NULL, NULL, NULL, DEFAULT, DEFAULT, $3::integer)`, [
            req.params.land, req.body.name, req.session.user.id
        ]);

        logsuccess(`The level ${req.body.name} of land ${req.params.land} has successfully been created !`);

        return {};
    }
}

export {
    LevelAdding
}