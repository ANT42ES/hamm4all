import { Page, API } from "../Page";
import {Request, Response} from "express";
import fs from "fs";
import { TextParser } from "./modules/TextParser/TextParser";

interface Load {
    readonly USER: object,
    readonly VERSION: string,
    readonly existsSync: Function,
    readonly textParse: Function,
    readonly API: API,
    readonly queries: object
}

abstract class GET extends Page {

    public abstract readonly PAGE: string;

    public async loadview(req: Request, res: Response, page: string): Promise<void>{
        const api = await this.getAPI(req, res);
        if (this.PAGE !== "none") {
            const param: Load = {
                USER: req.session.user,
                VERSION: req.session.changelog,
                existsSync: fs.existsSync,
                textParse: TextParser.parse,
                API: api,
                queries: req.query
            };
            if (! await this.apiErrorCheck(res, api)) {
                res.status(200).render(page, param);
            }
        }
        else {
            await this.apiErrorCheck(res, api);
        }
    }

    public async loadAPI(req: Request, res: Response): Promise<void>{
        const API = await this.getAPI(req, res);
        if (! await this.apiErrorCheck(res, API))
            res.status(200).send(API)
    }

}

export {
    GET
}

export * from "./GET/MainPage";
export * from "./GET/Lands";
export * from "./GET/Changelog";
export * from "./GET/ConnectPage";
export * from "./GET/Logout";
export * from "./GET/UserProfile";
export * from "./GET/Technics";
export * from "./GET/Crystals";
export * from "./GET/Letters";
export * from "./GET/CacheEnnemi";
export * from "./GET/Orders";
export * from "./GET/Bugs";
export * from "./GET/Fruits";
export * from "./GET/Boss";
export * from "./GET/SpecialObjects";
export * from "./GET/Families";
export * from "./GET/Quests";
export * from "./GET/Coefficients";
export * from "./GET/Pads";
export * from "./GET/Keys";
export * from "./GET/CreatedLevels";
export * from "./GET/Musics";
export * from "./GET/Contests";
export * from "./GET/Tools";
export * from "./GET/Thanks";
export * from "./GET/LandParts";
export * from "./GET/LevelsOfPart";
export * from "./GET/Levels";
export * from "./GET/ControlLevels";
export * from "./GET/Users";