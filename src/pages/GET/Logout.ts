import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";

class Logout extends GET {

    LINK = "/logout";
    PAGE = "none";

    public async getAPI(req: Request, res: Response): Promise<API> {
        req.session.user = undefined;
        res.redirect("/");
        return {};
    }

}

export {
    Logout
}