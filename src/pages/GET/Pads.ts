import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";

class Pads extends GET {

    LINK = "/objects/pads";
    PAGE = "objects/pads.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API;
        try {
            API = require(`../../../views/objects/pads.json`);
        }
        catch (err) {
            return ErrorCode.getAPIError(this.LINK, 10);
        }
        
        return API;
    }

}

export {
    Pads
}