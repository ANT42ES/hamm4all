
import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";

class Thanks extends GET {

    LINK = "/thanks";
    PAGE = "thanks.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {
        return {};
    }

}

export {
    Thanks
}