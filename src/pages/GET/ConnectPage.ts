import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";

class ConnectPage extends GET {

    LINK = "/connect";
    PAGE = "connect.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {
        return {};
    }

}

export {
    ConnectPage
}