import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { request_one_element } from "blessed-request";

class MainPage extends GET {

    LINK = "/";
    PAGE = "index.pug"

    public async getAPI(req: Request, res: Response): Promise<API> {
        req.session.changelog = await request_one_element("SELECT name FROM Changelog ORDER BY Date DESC LIMIT 1");
        return {};
    }
}

export {
    MainPage
}