
import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";

class Tools extends GET {

    LINK = "/creations/tools";
    PAGE = "creations/tools.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {
        return {};
    }

}

export {
    Tools
}