import fs from "fs";
import path from "path";
import { request, request_one_object, request_one_element, request_full } from "blessed-request";

import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { isNotANumber, isUndefined } from "../modules/verify";
import Hashids from "hashids";

class Levels extends GET {

    LINK = "/lands/:land/levels/:level";
    PAGE = "levels.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if(await isNotANumber(req.params.land, "/lands", res)) return ErrorCode.getAPIError(this.LINK, 11);
    
        const level: {previous_id: number, next_id: number, id: number, name: string} | undefined = await request_one_object("SELECT id, name, previous_id, next_id FROM Levels WHERE land_id = $1::integer AND name = $2::varchar", [req.params.land, req.params.level]);
        if (await isUndefined(level, `/lands`, res)) return ErrorCode.getAPIError(this.LINK, 13)
        
        const previous = await request_one_element("SELECT id, name FROM Levels WHERE id = $1::integer", [level.previous_id]);
        const next = await request_one_element("SELECT id, name FROM Levels WHERE id = $1::integer", [level.next_id]);
        const dims = await request_full("SELECT Dimension.name as dimname, Levels.name, id_destination_level, facing FROM Dimension INNER JOIN Levels ON Dimension.id_destination_level = Levels.id WHERE id_base_level = $1::integer", [level.id])
        const land = await request_one_object("SELECT id, name FROM Lands WHERE id = $1::integer", [req.params.land]);
        const levels = await request("SELECT id, name FROM Levels WHERE land_id = $1::integer ORDER BY name ASC", [req.params.land]);
        const description = await request_one_element("SELECT content FROM Descriptions WHERE id_level = $1::integer", [level.id]);
        const video = await request_one_element("SELECT link FROM Videos WHERE id_level = $1::integer", [level.id]);
        const fruits = await request_full("SELECT name, image FROM Fruits");
        const order = await request_full("SELECT Orders.id, Fruits.image, Fruits.name, order_num, x, y FROM Orders INNER JOIN Fruits ON Fruits.id = Orders.id_fruit WHERE id_level = $1::integer ORDER BY order_num ASC", [level.id])
    
        let API: any = {};
        API.land = land;
        API.land.levels = levels;
        API.level = {};
        API.level.name = level.name;
        API.level.next = next;
        API.level.previous = previous;
        API.level.dimensions = dims;
        API.level.description = description;
        API.level.video = video;
        API.level.fruits = fruits;
        API.level.order = order;
        API.level.screens = [];
    
        const hashids = new Hashids(req.params.land + req.params.level, 6);
    
        let i = 0;
        let screen_path = `/lands/${API.land.id}/${API.level.name}.png`;
        let encoded_screen_path = `/lands/${API.land.id}/${encodeURIComponent(API.level.name)}.png`;
        while (fs.existsSync(path.join(__dirname, "../../../../hamm4all-db", screen_path))) {
            API.level.screens.push({
                path: screen_path,
                encodedPath: encoded_screen_path
            });
            i++;
            const encoded = hashids.encode(i)
            screen_path = `/lands/${API.land.id}/${API.level.name}_${encoded}.png`;
            encoded_screen_path = `/lands/${API.land.id}/${encodeURIComponent(API.level.name)}_${encoded}.png`;
        }
    
        return API;
    }

}

export {
    Levels
}