import { request_full } from "blessed-request";

import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { isNotANumber } from "../modules/verify";

class ControlLevels extends GET {

    LINK = "/lands/:land/levels";
    PAGE = "controlLevels.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if(await isNotANumber(req.params.land, "/lands", res)) return ErrorCode.getAPIError(this.LINK, 12);
    
        const levels = request_full("SELECT id, name FROM Levels WHERE land_id = $1::integer ORDER BY name ASC", [req.params.land]);
        const API: API = {
            "id": req.params.land,
            "levels": await levels
        }

        return API;
    }

}

export {
    ControlLevels
}