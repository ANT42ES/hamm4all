import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";

class CreatedLevels extends GET {

    LINK = "/creations/levels";
    PAGE = "creations/levels.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API;
        try {
            API = require(`../../../views/creations/levels.json`);
        }
        catch (err) {
            return ErrorCode.getAPIError(this.LINK, 10);
        }
        
        return API;
    }

}

export {
    CreatedLevels
}