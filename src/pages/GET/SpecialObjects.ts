import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";

class SpecialObjects extends GET {

    LINK = "/objects/special";
    PAGE = "objects/special.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {
        return {};
    }

}

export {
    SpecialObjects
}