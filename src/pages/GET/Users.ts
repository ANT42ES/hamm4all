import { request_full } from "blessed-request";

import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { Level } from "../../types/Level";
import * as xp from "../modules/xp";

class Users extends GET {

    LINK = "/profile";
    PAGE = "users.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (! this.hasNecessaryRole(req.session.user, 5)) return ErrorCode.getAPIError(this.LINK, 2);

        const users: Array<{id: number, name: string, created_at: Date, role: number, experience: number, level: number}> =
            await request_full("SELECT id, name, created_at, role, experience FROM Users ORDER BY experience DESC");

        for(let user in users) {
            const LEVEL: Level = await xp.getExperienceStats(users[user].experience);
            users[user].level = LEVEL.lvl;
        }
        
        return users;
    }

}

export {
    Users
}