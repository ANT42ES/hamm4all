import { GET } from "../GET";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { logwarning } from "blessed-logcolor";
import { ErrorCode } from "../modules/ErrorCode";

class Letters extends GET {

    LINK = "/rules/letters";
    PAGE = "rules/letters.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API;
        try {
            API = require(`../../../views/rules/letters.json`);
        }
        catch (err) {
            return ErrorCode.getAPIError(this.LINK, 10);
        }
        
        return API;
    }

}

export {
    Letters
}