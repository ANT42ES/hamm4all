import { GET } from "../GET";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";

class CacheEnnemi extends GET {

    LINK = "/rules/cacheEnnemi";
    PAGE = "rules/cacheEnnemi.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API: API = {
            "img": "/page_data/cache-ennemi.png"
        }
        
        return API;
    }

}

export {
    CacheEnnemi
}