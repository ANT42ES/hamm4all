import { GET } from "../GET";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { logwarning } from "blessed-logcolor";
import { ErrorCode } from "../modules/ErrorCode";

class Crystals extends GET {

    LINK = "/rules/crystals";
    PAGE = "rules/crystals.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API;
        try {
            API = require(`../../../views/rules/crystals.json`);
        }
        catch (err) {
            return ErrorCode.getAPIError(this.LINK, 10);
        }
        
        return API;
    }

}

export {
    Crystals
}