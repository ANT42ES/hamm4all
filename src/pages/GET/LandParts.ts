import fs from "fs";
import path from "path";
import { request_one_object, request_one_element, request_full } from "blessed-request";

import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { isNotANumber, isUndefined } from "../modules/verify";

class LandParts extends GET {

    LINK = "/lands/:land";
    PAGE = "landParts.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (await isNotANumber(req.params.land, "/lands", res)) return ErrorCode.getAPIError(this.LINK, 11);
    
        let land: any | undefined = await request_one_object("SELECT id, name, released_at, description, avg(grade) as difficulty, has_variable_difficulty FROM Lands LEFT OUTER JOIN Land_Graduation ON Land_graduation.id_land = Lands.id WHERE id = $1::integer GROUP BY Lands.id", [req.params.land]);
    
        if (await isUndefined(land, "/lands", res)) return ErrorCode.getAPIError(this.LINK, 13);
    
        let landParts: Array<{id: number, name: string, difficulty: number, thumbnail_url: string}> | undefined = await request_full("SELECT id, name, difficulty FROM Land_Part WHERE id_land = $1::integer", [req.params.land]);
        if (landParts === undefined) landParts = [];
        const thumbnail_path = `/lands/${land.id}/image.png`;
        for(let i in landParts) {
            if (fs.existsSync(path.join(__dirname, "../../../../hamm4all-db", thumbnail_path))) {
                landParts[i].thumbnail_url = thumbnail_path;
            } else {
                landParts[i].thumbnail_url = `/assets/min/unknown-rounded.png`;
            } 
        };

        if (fs.existsSync(path.join(__dirname, "../../../../hamm4all-db", thumbnail_path))) {
            land.thumbnail_url = thumbnail_path;
        } else {
            land.thumbnail_url = `/assets/min/unknown-rounded.png`;
        }

        land.level_count = parseInt(await request_one_element("SELECT COUNT(*) FROM Levels WHERE land_id = $1::integer", [land.id]));
        land.parts = landParts;
    
        const user_land_grade = await request_one_element("SELECT grade FROM Land_Graduation WHERE id_user = $1::integer AND id_land = $2::integer", [(req.session.user) ? req.session.user.id : -5000, req.params.land]);
        land.gradedByYou = user_land_grade;

        land.authors = [];
        const authors: Array<{name: string}> = await request_full("SELECT name FROM Land_Authors INNER JOIN Authors ON Authors.id = Land_Authors.id_author WHERE id_land = $1::integer ORDER BY name ASC", [land.id]);
        authors.forEach(author => {
            land.authors.push(author.name);
        });
        
        return land;
    }

}

export {
    LandParts
}