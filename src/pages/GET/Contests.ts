
import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";

class Contests extends GET {

    LINK = "/creations/contests";
    PAGE = "creations/contests.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {
        return {};
    }

}

export {
    Contests
}