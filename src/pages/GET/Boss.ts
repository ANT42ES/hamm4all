import { GET } from "../GET";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { logwarning } from "blessed-logcolor";
import { ErrorCode } from "../modules/ErrorCode";

class Boss extends GET {

    LINK = "/enemies/boss";
    PAGE = "enemies/boss.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API: API = {
            "Tuberculoz": "/assets/favicon.png"
        };
        
        return API;
    }

}

export {
    Boss
}