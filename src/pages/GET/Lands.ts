import fs from "fs";
import path from "path";
import { request, request_one_element } from "blessed-request";

import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";

class Lands extends GET {

    LINK = "/lands";
    PAGE = "lands.pug"

    public async getAPI(req: Request, res: Response): Promise<API> {
        let LANDS: any = {};
        LANDS.lands = await request("SELECT id, name, avg(grade) as difficulty, has_variable_difficulty, released_at FROM Lands LEFT OUTER JOIN Land_Graduation ON Land_graduation.id_land = Lands.id GROUP BY lands.id ORDER BY avg(grade) ASC NULLS FIRST, name ASC");

        for (const index in LANDS.lands) {
            LANDS.lands[index].level_count = parseInt(await request_one_element("SELECT COUNT(*) FROM Levels WHERE land_id = $1::integer", [LANDS.lands[index].id]));
        }

        if (req.query && req.query.sortingMethod) {
            for(let index in this.SORTING_METHODS) {
                if (req.query.sortingMethod === this.SORTING_METHODS[index].query) {
                    LANDS.lands.sort(this.SORTING_METHODS[index].sort);
                }
            }
        }

        // for each land, add the good link to thumbnail, or a default link if none
        for(let i in LANDS.lands) {
            const thumbnail_path = `/lands/${LANDS.lands[i].id}/image.png`;
            if (fs.existsSync(path.join(__dirname, "../../../../hamm4all-db", thumbnail_path))) {
                LANDS.lands[i].thumbnail_url = thumbnail_path;
            } else {
                LANDS.lands[i].thumbnail_url = `/assets/min/unknown-rounded.png`;
            } 
        };

        LANDS.sorting_methods = this.SORTING_METHODS;
        LANDS.authors = await request("SELECT name FROM Authors ORDER BY name ASC");
        return LANDS;
    }

    private readonly SORTING_METHODS = [
        {
            "query": "difficulty",
            "name": "Tri par difficulté",
            "sort": (a: any, b: any) => a.difficulty - b.difficulty
        },
        {
            "query": "alphabetical",
            "name": "Tri par ordre alphabétique",
            "sort": (a: any, b: any) => a.name.localeCompare(b.name)
        },
        {
            "query": "id_based",
            "name": "Tri par id de contrée",
            "sort": (a: any, b: any) => a.id - b.id
        },
        {
            "query": "release_date",
            "name": "Tri par date de sortie",
            "sort": (a: any, b: any) => a.released_at - b.released_at
        },
        {
            "query": "level_count",
            "name": "Tri par nombre de niveaux",
            "sort": (a: any, b: any) => b.level_count - a.level_count
        },
    ];

}

export {
    Lands
}