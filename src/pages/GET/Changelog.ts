import { request, request_one_object, request_one_element, request_full } from "blessed-request";

import { GET } from "../GET";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";

class Changelog extends GET {

    LINK = "/changelog";
    PAGE = "changelog.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {
        if (! this.hasNecessaryRole(req.session.user, 1)) return ErrorCode.getAPIError(this.LINK, 1);
        const Changelog = request("SELECT name, date, changes FROM Changelog ORDER BY date DESC");
        return Changelog;
    }

}

export {
    Changelog
}