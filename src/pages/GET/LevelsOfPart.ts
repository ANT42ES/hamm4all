import { request, request_one_object, request_one_element, request_full } from "blessed-request";

import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { isNotANumber, isUndefined } from "../modules/verify";

class LevelsOfPart extends GET {

    LINK = "/lands/:land/parts/:part";
    PAGE = "levelsOfPart.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        const redirectPath = `lands/${req.params.land}`;

        if(await isNotANumber(req.params.land, "/lands", res)) return ErrorCode.getAPIError(this.LINK, 12);
        if(await isNotANumber(req.params.part, "/lands", res)) return ErrorCode.getAPIError(this.LINK, 12);

        const API: any = {};
        const land: object | undefined = await request_one_object("SELECT id, name FROM Lands WHERE id = $1::integer", [req.params.land]);
        if (await isUndefined(land, redirectPath, res)) return ErrorCode.getAPIError(this.LINK, 13);

        let id_part: number = (await request("SELECT id FROM Land_Part WHERE id_land = $1::integer", [req.params.land]))[parseInt(req.params.part)];
        if (await isNotANumber(`${id_part}`, redirectPath, res)) return ErrorCode.getAPIError(this.LINK, 13);

        const id_land_part: number = id_part;
        const land_part: object = await request_one_object("SELECT id, name, difficulty, description FROM Land_Part WHERE id = $1::integer", [id_land_part]);

        // Contains all the sequences that are shown on the page
        let thumbnail_sequence = await request_full("SELECT Thumbnail_Sequence.id, id_start, levels.name as start_level_name, order_num FROM Thumbnail_Sequence INNER JOIN Levels on Levels.id = Thumbnail_Sequence.id_start WHERE id_land_part = $1::integer ORDER BY order_num ASC", [id_land_part]);
        let flag: Array<"well" | "dim" | "laby"> = [];
        let name: Array<string> = [];
        for(let seq of thumbnail_sequence) {
            // Fetch the ID of the first level in the links
            const firstLev: number = await request_one_element("SELECT id_level FROM Thumbnail_Link WHERE id_seq = $1::integer GROUP BY id_level, order_num HAVING order_num <= ALL (SELECT order_num FROM Thumbnail_link WHERE id_seq = $2::integer)", [seq.id, seq.id]);

            const isDim = await request_one_element(`SELECT name FROM Dimension WHERE id_base_level = $1::integer AND id_destination_level = $2::integer`, [seq.id_start, (firstLev) ? firstLev : 0]);
            const isLaby = await request_one_element(`SELECT (coordinates).x FROM Thumbnail_Link WHERE id_seq = $1::integer LIMIT 1`, [seq.id]);

            // Determine if laby, dim, or well
            if (isLaby !== null && isLaby !== undefined) {
                name.push(isDim)
                flag.push("laby");
            }
            else if (isDim !== undefined) {
                name.push(isDim)
                flag.push("dim");
            }
            else {
                name.push(undefined)
                flag.push("well");
            }
        }

        let thumb_link: any = [];
        for(let elem of thumbnail_sequence) {
            const res = await request(`SELECT name, (coordinates).* FROM Thumbnail_link INNER JOIN Levels ON Levels.id = Thumbnail_Link.id_level WHERE id_seq = $1::integer ORDER BY order_num ASC`, [elem.id]);
            thumb_link.push(res);
        }

        API.land = land;
        API.part = land_part;
        for (let seq in thumbnail_sequence) {
            const i: number = parseInt(seq)
            thumbnail_sequence[i].type = flag[i];
            thumbnail_sequence[i].levels = thumb_link[i];
            thumbnail_sequence[i].sequence_name = name[i];
        }
        API.part.sequences = thumbnail_sequence;
        const levels = request("SELECT id, name FROM Levels WHERE land_id = $1::integer ORDER BY name ASC", [req.params.land]);
        API.land.levels = await levels;

        return API;
    }

}

export {
    LevelsOfPart
}