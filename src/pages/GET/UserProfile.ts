import fs from "fs";
import path from "path";
import { request, request_one_object, request_one_element, request_full } from "blessed-request";

import { GET } from "../GET";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";
import { isNotANumber, isUndefined } from "../modules/verify";
import { Level } from "../../types/Level";
import * as xp from "../modules/xp";

class UserProfile extends GET {

    LINK = "/profile/:user";
    PAGE = "user_profile.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        if (req.session.user === undefined) return ErrorCode.getAPIError(this.LINK, 1);
        if (req.session.user.id != req.params.user && req.session.user.role < 3) return ErrorCode.getAPIError(this.LINK, 2);
        
        if(await isNotANumber(req.params.user, "/", res)) return ErrorCode.getAPIError(this.LINK, 9);
        
        const profile: {experience: number, name: string, created_at: Date, role: number} | undefined = await request_one_object("SELECT name, experience, created_at, role FROM Users WHERE id = $1::integer", [req.params.user]);
        if(await isUndefined(profile, "/", res)) return ErrorCode.getAPIError(this.LINK, 9);
        
        const exp: Level = await xp.getExperienceStats(profile.experience);
        
        let pic_url = `/users/${req.params.user}/image.png`;
        if (! fs.existsSync(path.join(__dirname, "../../../../hamm4all-db", pic_url))) pic_url = "/assets/min/unknown-rounded.png"
        
        let API: {id: string, name: string, experience: number, created_at: Date, role: number, lvl: number, xp: number, before: number, picture_url: string} = {
            id: req.params.user,
            name: profile.name,
            experience: profile.experience,
            created_at: profile.created_at,
            role: profile.role,
            lvl: exp.lvl,
            xp: exp.xp,
            before: exp.before,
            picture_url: pic_url
        };
        
        return API;
    }

}

export {
    UserProfile
}