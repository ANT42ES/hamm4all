import { GET } from "../GET";
import { API, APIError } from "../../Page";
import { Request, Response } from "express";
import { logwarning } from "blessed-logcolor";
import { ErrorCode } from "../modules/ErrorCode";

class Orders extends GET {

    LINK = "/rules/orders";
    PAGE = "rules/orders.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API;
        try {
            API = require(`../../../views/rules/orders.json`);
        }
        catch (err) {
            return ErrorCode.getAPIError(this.LINK, 10);
        }
        
        return API;
    }

}

export {
    Orders
}