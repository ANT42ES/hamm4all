import { GET } from "../GET";
import { API } from "../../Page";
import { Request, Response } from "express";
import { ErrorCode } from "../modules/ErrorCode";

class Musics extends GET {

    LINK = "/creations/musics";
    PAGE = "creations/musics.pug";

    public async getAPI(req: Request, res: Response): Promise<API> {

        let API;
        try {
            API = require(`../../../views/creations/musics.json`);
        }
        catch (err) {
            return ErrorCode.getAPIError(this.LINK, 10);
        }
        
        return API;
    }

}

export {
    Musics
}