import {request} from "blessed-request";

const firstLevel: number = 10;

import { Level } from "../../types/Level";

/**
 * Function that adds XP to an user
 * @param {id} user user id 
 * @param {number} nb amount of XP that is given (INTEGER ONLY) 
 */
async function add(user: number, nb: number = 1): Promise<void> {
    request("UPDATE Users SET experience = experience + $1::integer WHERE id = $2::integer", [nb, user]);
}

/**
 * Function that sets XP to an user
 * @param {id} user user id 
 * @param {number} nb number of XP that is given (INTEGER ONLY) 
 */
async function set(user: number, nb: number = 0): Promise<void> {
    request("UPDATE Users SET experience = $1::integer WHERE id = $2::integer", [nb, user]);
}

/**
 * Function that gives stats with an experience
 * @param {number} exp 
 */
async function getExperienceStats(exp: number): Promise<Level> {
    let level: Level = new Level(1, firstLevel, 0);
    while (level.xp <= exp) {
        level.before = level.xp;
        level.xp = Math.round(level.xp * (level.lvl + 1) / level.lvl + 3 * level.lvl * level.lvl);
        level.lvl++;
    }
    return level;
}

export {
    add,
    getExperienceStats,
    set
}