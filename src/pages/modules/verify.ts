import { Response } from "express";
import { logerror } from "blessed-logcolor";

/**
 * Function that tells the user if an expression is undefined
 * @param expression expression
 * @param redirection redirection
 * @param res res
 */
async function isUndefined(expression: any | undefined, redirection: string = "/", res: Response) : Promise<boolean> {
    if (expression === undefined) {
        logerror(`The expression "${expression}" is undefined`);
        return true;
    }
    return false;
}

/**
 * Function that tells the user if an expression is not a number
 * @param expression expression
 * @param redirection redirection
 * @param res res
 */
async function isNotANumber(expression: string, redirection: string = "/", res: Response) : Promise<boolean> {
    if (isNaN(parseInt(expression))) {
        logerror(`The expression "${expression}" is not a number`);
        return true;
    }
    else {
        return false;
    }
}

export {
    isUndefined,
    isNotANumber
}