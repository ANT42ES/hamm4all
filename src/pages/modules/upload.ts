import { logsuccess, logerror } from "blessed-logcolor";
import sharp from "sharp";

const validExtension: string[] = ["png", "jpg"];

async function verify(name: string): Promise<boolean> {
    const ext: string | undefined = await getExtension(name);
    return (! validExtension.includes(ext));
}

async function getExtension(name: string): Promise<string | undefined> {
    const splited = name.split(".");
    if (splited.length <= 1) {
        return undefined;
    }
    return splited[splited.length - 1].toLowerCase();
}

async function resize(data: Buffer | string, x: number, y: number, toFile: string, success: string, error: string) {
    sharp(data)
    .resize(x, y)
    .toFile(toFile)
    .then(() => logsuccess(success))
    .catch((err: any) => logerror(`${error} : \n${err}`))
}

export {
    verify,
    resize,
    getExtension,
    validExtension
}