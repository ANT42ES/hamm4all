const MAX: number = 10;
const MIN: number = 0;

async function difficulty(difficulty: string): Promise<number> {
    let res: number;
    if (isNaN(parseInt(difficulty))) {
        res = undefined;
    }
    else {
        res = parseFloat(difficulty);
        if (res < MIN) {
            res = MIN;
        }
        else if (res > MAX) {
            res = MAX;
        }
    }
    return res;
}

export {
    difficulty,
    MIN,
    MAX
}