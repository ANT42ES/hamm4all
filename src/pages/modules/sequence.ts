async function get(arr: number[] | undefined): Promise<number> {
    let i: number = 0;
    if (arr && arr.length > 0) {
        arr.sort((a, b) => a - b);
        let coeff: number = arr[0];
        while(arr[i] === coeff) {
            i++;
            coeff++;
        }
        return coeff;
    }
    else {
        return 1;
    }
}

export {
    get
}