
import { Link } from "./Link";
import { Mark, MarkOptions } from "./Mark";

interface MarkupPart {
    "start": number,
    "end": number
}

interface Markup {
    "opening": MarkupPart,
    "closing": MarkupPart
}

interface Marks {
    [index: string]: Mark
}

class TextParser {

    private static readonly CALL_CHAR = "§";
    private static readonly MARKUP_CLOSING = {
        "start": "[",
        "end": "]"
    };

    private static readonly MARKS: Marks = {
        "link": new Link()
    }

    public static parse(text: string): string {
        const letters: Array<string> = text.split("");
        const copy: Array<string> = text.split("");
        for (const ind in letters) {
            let index: number = parseInt(ind);
            if (letters[index] === this.CALL_CHAR) {
                const keyword: string = this.getKeyword(letters, index);
                if (this.MARKS[keyword] !== undefined) {
                    const options: MarkOptions = this.getAttributes(letters, index);
                    const result: string = this.MARKS[keyword].parse(options);
                    const end: number = this.getEnd(letters, index);
                    copy.splice(index, end + 1);
                    copy.splice(index, 0, result);
                }
            }
        }
        return copy.join();
    }

    private static getKeyword(letters: Array<string>, ind: number): string {
        ind++;
        let keyword: string = "";
        if (letters[ind] === this.MARKUP_CLOSING.start) {
            ind++;
            while (letters[ind] !== " ") {
                keyword += letters[ind];
                ind++;
                if(ind > 10000) throw new Error("endless loop");
            }
        }
        return keyword;
    }

    private static getAttributes(letters: Array<string>, ind: number): MarkOptions {
        ind++;
        const end: number = this.getEnd(letters, ind);
        const attributes: Array<string> = letters.splice(ind, end - ind).join().split(" ");
        attributes.pop();
        let options: MarkOptions = {};
        for(const att of attributes) {
            const splitted = att.split("=");
            options[splitted[0]] = splitted[1];
        }
        return options;
    }

    private static getEnd(letters: Array<string>, ind: number): number {
        ind++;
        while (letters[ind] !== this.MARKUP_CLOSING.end) {
            ind++;
        }
        return ind
    }
}

export {
    TextParser
}