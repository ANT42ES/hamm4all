import { Mark, MarkOptions } from "./Mark";

class Link extends Mark {

    public parse(options: MarkOptions): string {
        let href: string;
        for (const [key, value] of Object.entries(options)) {
            if (key === "href") {
                href = value;
            }
        }
        return `<a href="${href}">${options.value}</a>`
    }

}

export {
    Link
}