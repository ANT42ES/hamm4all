interface MarkOptions {
    [index: string]: string
}

abstract class Mark {

    public abstract parse(options: MarkOptions): string

}

export {
    Mark,
    MarkOptions
}