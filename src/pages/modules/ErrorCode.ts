import {APIError} from "../../Page";
import { logwarning } from "blessed-logcolor";

interface ErrorKnowledge {
    message: string;
    HTTPCode: number;
}

class ErrorCode {

    private static readonly ERROR_CODE: Array<ErrorKnowledge> = [
        { message: "Unknown error", HTTPCode: 500 },
        { message: "The user is not authenticated", HTTPCode: 401 },
        { message: "Access rights not high enough", HTTPCode: 403 },
        { message: "This username does not exist", HTTPCode: 400 },
        { message: "This password does not correspond to the associated username", HTTPCode: 400 },
        { message: "This user has already generated a token", HTTPCode: 400 },
        { message: "A problem has occured during the save/load of a file", HTTPCode: 500 },
        { message: "This token does not exist", HTTPCode: 500 },
        { message: "This username already exists", HTTPCode: 400 },
        { message: "This profile ID does not exist", HTTPCode: 400 },
        { message: "The associated JSON hasn't been loaded properly", HTTPCode: 500 },
        { message: "One or multiple params ID are not a number", HTTPCode: 400},
        { message: "One or multiple params are undefined", HTTPCode: 400},
        { message: "The requested entity by the given data does not exist", HTTPCode: 400},
        { message: "The file you are trying to upload is over the size limit", HTTPCode: 400},
        { message: "The file you are trying to upload is not an image", HTTPCode: 400},
        { message: "The description is too short", HTTPCode: 400},
        { message: "One of the options passed is has not a correct value", HTTPCode: 500},
        { message: "One of multiple values are out of their bounds", HTTPCode: 400},
    ];

    public static get(index: number = 0): ErrorKnowledge {
        const error: ErrorKnowledge = this.ERROR_CODE[index];
        return error;
    }

    public static getAPIError(path: string, error_id: number, logMethod: (log: string) => void = logwarning): APIError  {
        const errorKnowledge: ErrorKnowledge = ErrorCode.get(error_id);
        logMethod(`"${path}": ${errorKnowledge.message}`);
        const error: APIError = {"error": true, "message": errorKnowledge.message, "error_id": error_id};
        return error;
    }

}



export {
    ErrorKnowledge,
    ErrorCode
}