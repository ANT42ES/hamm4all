import { request_one_element } from "blessed-request";

async function fetchLevelID(id_land: number | string, level_name: string): Promise<number | undefined> {
    return request_one_element("SELECT id FROM Levels WHERE land_id = $1::integer AND name = $2", [id_land, level_name]);
}

export {
    fetchLevelID
}