/**
 * Donne la date locale sous la forme DD/MM/YYYY
 * @returns chaîne de caracère contenant la date
 */
exports.localeDateDDMMYYYY = function(){
    let date = new Date();
    return date.toLocaleDateString("fr-FR");
}

/**
 * Donne la date courante au format ISO
 * @returns chaîne de caracère contenant la date
 */
exports.dateISO = function(){
    let date = new Date();
    return date.toISOString();;
}

/**
 * Donne la date donnée sous la forme DD/MM/YYYY
 * @param d date à convertir
 * @returns chaîne de caracère contenant la date
 */
exports.dateDDMMYYYY = function(d: Date){
    return d.toLocaleDateString("fr-FR");
}

/**
 * Donne la date donnée sous la forme HH:MM:SS
 * @param d date à convertir
 * @returns chaîne de caracère contenant la date
 */
exports.dateHHMMSS = function(d: Date){
    return d.toLocaleTimeString("fr-FR");
}
