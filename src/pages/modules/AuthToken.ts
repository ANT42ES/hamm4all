import { APIError } from "../../Page";
import { ErrorCode } from "./ErrorCode";

import fs from "fs";
import path from "path";
import Hashids from "hashids";

interface AuthTokenInfo {
    playerID: number;
    playerName: string;
    role: number;
}

class AuthToken {

    private static readonly LENGTH = 16;
    private static readonly AUTH_TOKENS_PATH = "../../../data/.authTokens.json";

    static async generate(): Promise<string> {
        const numbers: Array<number> = await this.getRandomizedNumbersInArray(this.LENGTH);
        const rawHash: string = new Hashids("AuthToken", this.LENGTH).encode(numbers);
        return (rawHash.length > this.LENGTH) ? rawHash.substring(0, this.LENGTH) : rawHash;
    }

    private static async getRandomizedNumbersInArray(length: number = 1): Promise<Array<number>> {
        let res: Array<number> = [];
        for(let i = 0 ; i < length ; i++) {
            res.push(Math.floor(Math.random() * (2 << 8)));
        }
        return res;
    }

    static async save(token: string, playerID: number, nameOfPlayer: string, role: number): Promise<APIError | void> {
        try {
            const save = require(this.AUTH_TOKENS_PATH);
            for (const [, value] of Object.entries(save)) {
                let val: AuthTokenInfo = (value as AuthTokenInfo);
                if (val.playerName === nameOfPlayer) {
                    return ErrorCode.getAPIError("/login", 5);
                }
            }
            const info: AuthTokenInfo = {playerID: playerID, playerName: nameOfPlayer, role: role};
            save[token.toString()] = info;
            fs.writeFileSync(path.join(__dirname, this.AUTH_TOKENS_PATH), JSON.stringify(save));
        }
        catch(err) {
            return ErrorCode.getAPIError("/login", 6);
        }
    }

    static async getTokenInfo(token: string): Promise<AuthTokenInfo | APIError> {
        try {
            const save = require(this.AUTH_TOKENS_PATH);
            const info: AuthTokenInfo | undefined = save[token.toString()];
            if (info === undefined) {
                return ErrorCode.getAPIError("", 7);
            }
            return info;
        }
        catch (err) {
            return ErrorCode.getAPIError("", 6);
        }
    }

    static async delete(token: string): Promise<APIError | void> {
        try {
            const save = require(this.AUTH_TOKENS_PATH);
            save[token.toString()] = undefined;
            fs.writeFileSync(path.join(__dirname, this.AUTH_TOKENS_PATH), JSON.stringify(save));
        }
        catch (err) {
            return ErrorCode.getAPIError("", 6);
        }
    }

    static async exists(name: string): Promise<APIError | boolean> {
        try {
            const save = require(this.AUTH_TOKENS_PATH);
            for (const [, value] of Object.entries(save)) {
                let val: AuthTokenInfo = (value as AuthTokenInfo);
                if (val.playerName === name) {
                    return true;
                }
            }
            return false;
        }
        catch (err) {
            return ErrorCode.getAPIError("", 6);
        }
    }

    static async getTokenByPlayerName (name: string): Promise<APIError | string> {
        try {
            const save = require(this.AUTH_TOKENS_PATH);
            for (const [key, value] of Object.entries(save)) {
                let val: AuthTokenInfo = (value as AuthTokenInfo);
                if (val.playerName === name) {
                    return key;
                }
            }
            return ErrorCode.getAPIError("", 7);
        }
        catch (err) {
            return ErrorCode.getAPIError("", 6);
        }
    }

}

export {
    AuthToken,
    AuthTokenInfo
}