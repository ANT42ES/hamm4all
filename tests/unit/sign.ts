import { request } from "blessed-request";
import * as POST from "../../src/public/pages/POST";
import { logout } from "../../src/public/pages/GET";
import { expect } from 'chai';

let res: any = {
    "redirect": function(a: string) { return a }
};
let req: any = {};
req.session = {};
req.body = {};
req.query = {};

const NAME: string = "oooooooo";

describe("POST.ts", async () => {

    describe("sign up", () => {
    
        it(`Signing up`, async () => {
            req.body.username = NAME;
            req.body.password = "ah"
            const result = await POST.signUp(req, res);
            expect(result).to.equal(undefined);
        });

        it(`Username has already been taken`, async () => {
            req.body.password = "oh"
            const result = await POST.signUp(req, res);
            expect(result).to.equal("/connect")
        });
    });
    
    describe("sign in", () => {

        it(`Account doesn't exist`, async () => {
            req.body.username = "jhfgcikuyirw";
            req.body.password = "ah";
            const result = await POST.connection(req, res);
            expect(result).to.equal("/connect")
        });

        
        it(`Wrong password`, async () => {
            req.body.username = NAME;
            req.body.password = "oh";
            const result = await POST.connection(req, res);
            expect(result).to.equal(undefined);
        });

        
        it(`Connect !`, async () => {
            req.body.password = "ah";
            const result = await POST.connection(req, res);
            expect(result).to.equal("/");
            expect(req.session.user.name).to.equal(NAME)
        });
    });

    describe("logout", async () => {
    
        it(`should redirect`, async () => {
            const result = await logout(req, res);
            expect(result).to.equal(undefined)
            expect(req.session.user).to.equal(undefined);
        });
        
    });

    after(async () => {
        request("DELETE FROM Users WHERE name = $1::varchar", [NAME])
    })
}); 