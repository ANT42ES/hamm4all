import * as sequence from "../../src/public/pages/parse/sequence"
import { expect } from 'chai';

describe("getSequence", async () => {

    const values = [
        {"value": [1], "expected": 2},
        {"value": [0], "expected": 1},
        {"value": [-3, -2], "expected": -1},
        {"value": [1, 2, 4, 5], "expected": 3},
        {"value": [2, 3, 5], "expected": 4},
        {"value": [-2, 5, 1], "expected": -1},
        {"value": [7, 3, 2, 1], "expected": 4},
        {"value": [1, 1, 2, 2], "expected": 2},
        {"value": undefined, "expected": 1}
    ];

    values.forEach(val => {
        it(`should be ${val.expected}`, async () => {
            const res = await sequence.get(val.value);
            expect(res).to.equal(val.expected)
        });
    });
    
});