import * as id from "../../src/public/pages/parse/id"
import { expect } from 'chai';

const OFFICIALFEST: number = 31;

describe("fetchLevelID", async () => {

    const values = [
        {"value": OFFICIALFEST, "value2": "0", "expected": 1387},
        {"value": `${OFFICIALFEST}`, "value2": "54.12.1D.7", "expected": 1630},
        {"value": 9000, "value2": "0", "expected": undefined},
    ]

    values.forEach(val => {
        it(`should be ${val.expected}`, async () => {
            const res = await id.fetchLevelID(val.value, val.value2);
            expect(res).to.equal(val.expected)
        });
    });
    
});