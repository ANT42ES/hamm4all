import * as upload from "../../src/public/pages/parse/upload"
import { expect } from 'chai';

describe("upload.ts", async () => {
    describe("getExtension", async () => {

        const values = [
            {"value": "yes", "expected": undefined},
            {"value": "nono.png", "expected": "png"},
            {"value": "flowroot.jpg", "expected": "jpg"},
            {"value": "virus.exe.png", "expected": "png"},
            {"value": "morse...mor", "expected": "mor"}
        ];
    
        values.forEach(val => {
            it(`should be ${val.expected}`, async () => {
                const res = await upload.getExtension(val.value);
                expect(res).to.equal(val.expected)
            });
        });
        
    });
    
    describe("verify", async () => {
    
        const values = [
            {"value": "yes", "expected": true},
            {"value": "nono.png", "expected": false},
            {"value": "flowroot.jpg", "expected": false},
            {"value": "virus.exe.png", "expected": false}, // TODO : Fix this security issue
            {"value": "morse...mor", "expected": true}
        ];
    
        values.forEach(val => {
            it(`should be ${val.expected}`, async () => {
                const res = await upload.verify(val.value);
                expect(res).to.equal(val.expected)
            });
        });
        
    });
}); 
