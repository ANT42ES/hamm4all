import * as difficulty from "../../src/public/pages/parse/difficulty"
import { expect } from 'chai';

describe("difficulty", async () => {

    const values = [
        {"value": `${difficulty.MIN - 3}`, "expected": difficulty.MIN},
        {"value": `${2}`, "expected": 2},
        {"value": `${difficulty.MAX + 3}`, "expected": difficulty.MAX},
        {"value": "oui", "expected": undefined}
    ]

    values.forEach(val => {
        it(`should be ${val.expected}`, async () => {
            const diff = await difficulty.difficulty(val.value)
            expect(diff).to.equal(val.expected)
        });
    });
    
});